package com.bank.dao;

import java.util.List;

import com.bank.model.Login;

public interface LoginDaoIntf {

	void saveEmployee(Login login);

	List<Login> fetchData();

}
