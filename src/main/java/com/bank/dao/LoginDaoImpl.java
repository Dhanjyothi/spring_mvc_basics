package com.bank.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bank.model.Login;
@Repository
public class LoginDaoImpl implements LoginDaoIntf{

	@Autowired
	SessionFactory sessionFactory;
	public void saveEmployee(Login login) {
		sessionFactory.openSession().save(login);
		
	}
	public List<Login> fetchData() {
		
		return sessionFactory.openSession().createQuery("from Login").list();
	}

}
