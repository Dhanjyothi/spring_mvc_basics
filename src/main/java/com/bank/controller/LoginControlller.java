package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMethodMappingNamingStrategy;

import com.bank.model.Login;
import com.bank.service.LoginServiceIntf;

@Controller
public class LoginControlller {

	@Autowired
	LoginServiceIntf service;
	//@RequestMapping(value="/Success",method=RequestMethod.POST)
	@PostMapping(value="/Success")
	public ModelAndView welcomePage(@Validated @ModelAttribute("login") Login login,BindingResult result) {
		if(result.hasErrors())
		{
		return new ModelAndView("Login");//Jsp
		}
		service.saveLogin(login);
		return new ModelAndView("redirect:/fetchDeatils");//Jsp
	}
	//@RequestMapping(value="/Login",method=RequestMethod.GET)
	@GetMapping(value="/Login")
	public ModelAndView loginPage(@ModelAttribute("login") Login login) {
		
		return new ModelAndView("Login");//Jsp
	}
	//@RequestMapping(value="/fetchDeatils",method=RequestMethod.GET)
	@GetMapping(value="/fetchDeatils")
	public ModelAndView fetchPage(@ModelAttribute("login") Login login) {
		
		List<Login> ls=service.fetchData();
		return new ModelAndView("Success","login",ls);//Jsp
		
	}
}
