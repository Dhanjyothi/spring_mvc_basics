package com.bank.service;

import java.util.List;

import com.bank.model.Login;

public interface LoginServiceIntf {

	void saveLogin(Login login);

	List<Login> fetchData();

}
