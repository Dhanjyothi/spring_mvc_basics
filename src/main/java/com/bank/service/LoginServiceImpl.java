package com.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.dao.LoginDaoIntf;
import com.bank.model.Login;

@Service
@Transactional
public class LoginServiceImpl implements LoginServiceIntf{

	@Autowired
	LoginDaoIntf dao;
	public void saveLogin(Login login) {
		dao.saveEmployee(login);
		
	}
	public List<Login> fetchData() {
		
		return dao.fetchData();
	}

}
